import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, ErrorStateMatcher } from '@angular/material';
import { FormGroupDirective, Validators, FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { getHours, format } from 'date-fns/esm';
import { Booking, BookingService } from '../services/booking/booking.service';
import { getDay, getDate } from 'date-fns/esm/fp';
import { getMonth } from 'date-fns/fp';
import { getYear } from 'date-fns';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-booking-dialog',
  templateUrl: './booking-dialog.component.html',
  styleUrls: ['./booking-dialog.component.scss']
})
export class BookingDialogComponent {

  titleFormControl: FormControl;
  fromFormControl: FormControl;
  toFormControl: FormControl;
  matcher: MyErrorStateMatcher;

  private fromTime: string;
  private toTime: string;

  constructor(
    public dialogRef: MatDialogRef<BookingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public freeRoom: FreeRoom,
    private authService: AuthService, private bookingService: BookingService
  ) {
    this.fromTime = format(this.freeRoom.from, 'HHmm');
    this.toTime = format(this.freeRoom.to, 'HHmm');

    this.titleFormControl = new FormControl('', [
      Validators.required
    ]);
    this.fromFormControl = new FormControl('', [
      Validators.required,
      Validators.min(+this.fromTime),
      Validators.max(+this.toTime)
    ]);
    this.toFormControl = new FormControl('', [
      Validators.required,
      Validators.min(+this.fromTime),
      Validators.max(+this.toTime)
    ]);
    this.matcher = new MyErrorStateMatcher();

    this.fromFormControl.setValue(this.fromTime);
    this.toFormControl.setValue(this.toTime);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  book() {
    const temp = new Booking();
    temp.roomId = this.freeRoom.name;
    temp.day = getDate(this.freeRoom.from);
    temp.month = getMonth(this.freeRoom.from) + 1;
    temp.year = getYear(this.freeRoom.from);
    temp.title = this.titleFormControl.value;
    temp.userId = this.authService.user$.value.userId;
    temp.fromTime = this.fromFormControl.value;
    temp.toTime = this.toFormControl.value;

    this.dialogRef.close(temp);
  }

}

class FreeRoom {
  constructor(
    public name: string,
    public from: Date,
    public to: Date
  ) { }
}

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
