import { ApiService, ApiRoomEvent } from './../services/api/api.service';
import { Component, OnInit, Inject } from '@angular/core';
import { endOfDay, subMinutes, isSameDay } from 'date-fns/esm';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators/switchMap';
import { of } from 'rxjs/observable/of';
import { empty } from 'rxjs/observable/empty';
import { Observable } from 'rxjs/Observable';
import { BookingService, Booking } from '../services/booking/booking.service';
import _collection from 'lodash-es/collection';
import _array from 'lodash-es/array';
import _lang from 'lodash-es/lang';
import { from } from 'rxjs/observable/from';
import { map } from 'rxjs/operators/map';
import { concatMap } from 'rxjs/operators/concatMap';
import { startOfDay } from 'date-fns/esm/fp';
import { addDays } from 'date-fns';
import { toArray } from 'rxjs/operators/toArray';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { User, AuthService } from '../services/auth/auth.service';
import { MatDialog } from '@angular/material';
import { BookingDialogComponent } from '../booking-dialog/booking-dialog.component';

@Component({
  selector: 'app-free-rooms',
  templateUrl: './free.component.html',
  styleUrls: ['./free.component.scss']
})
export class FreeRoomsComponent implements OnInit {

  events: ApiRoomEvent[];
  bookings: Booking[];
  roomArray: string[];
  freeRoomsArray: FreeRoom[];
  occupiedTimesArray: OccupiedTimes[];
  startTime: Date;
  startTime$: Observable<Date>;
  user$: BehaviorSubject<User>;

  constructor(
    private route: ActivatedRoute, private router: Router,
    private dialog: MatDialog,
    private apiService: ApiService, private bookingService: BookingService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.events = [];
    this.bookings = [];
    this.roomArray = [];
    this.startTime = new Date();
    this.freeRoomsArray = [];
    this.occupiedTimesArray = [];

    this.user$ = this.authService.user$;
    this.apiService.apiRoomEvents$.subscribe((apiRoomEvents: ApiRoomEvent[]) => {
      this.addUniOccupations(apiRoomEvents);
      this.freeRooms();
    });
    this.apiService.universityRooms$.subscribe((roomArray: string[]) => {
      this.addRooms(roomArray);
      this.freeRooms();
    });
    this.bookingService.bookings$.subscribe((bookings: Booking[]) => {
      this.addBookinOccupations(bookings);
      this.freeRooms();
    });
    this.bookingService.bookingRooms$.subscribe((roomArray: string[]) => {
      this.addRooms(roomArray);
      this.freeRooms();
    });
    this.startTime$ = this.route.paramMap
      .pipe(
      switchMap((params: ParamMap) => {
        const temp = params.get('from');
        this.startTime = new Date(temp);
        // console.log('Param from: ', this.startTime);
        if (temp) {
          return of(this.startTime);
        } else {
          this.router.navigate(['/free', { from: new Date().toISOString() }]);
          return empty();
        }
      }));
  }

  private addUniOccupations(roomEvents: ApiRoomEvent[]) {
    const temp: OccupiedTimes[] = [];
    for (const event of roomEvents) {
      temp.push(new OccupiedTimes(event.Room, event.FromDate, event.ToDate));
    }
    this.occupiedTimesArray = this.occupiedTimesArray.concat(temp);
    this.occupiedTimesArray = _array.uniqWith(this.occupiedTimesArray, _lang.isEqual);
  }

  private addBookinOccupations(bookings: Booking[]) {
    const temp: OccupiedTimes[] = [];
    for (const booking of bookings) {
      temp.push(new OccupiedTimes(booking.roomId, booking.fromDate, booking.toDate));
    }
    this.occupiedTimesArray = this.occupiedTimesArray.concat(temp);
    this.occupiedTimesArray = _array.uniqWith(this.occupiedTimesArray, _lang.isEqual);
  }

  private addRooms(rooms: string[]) {
    this.roomArray = this.roomArray.concat(rooms);
    this.roomArray = _collection.sortBy(this.roomArray);
    this.roomArray = _array.sortedUniq(this.roomArray);
  }

  private freeRooms() {
    this.freeRoomsArray = [];

    from(this.roomArray).pipe(
      map((room: string) => {
        const roomOccupations = this.occupiedTimesArray.filter(
          (occupation: OccupiedTimes) => occupation.roomId === room && occupation.to > this.startTime);
        return { room: room, occupations: _collection.sortBy(roomOccupations, (occupation: OccupiedTimes) => occupation.from) };
      }),
      concatMap((data: { room: string, occupations: OccupiedTimes[] }) => {
        const tempFreeRooms: FreeRoom[] = [];
        if (data.occupations.length === 0) {
          tempFreeRooms.push(new FreeRoom(data.room, this.startTime, endOfDay(this.startTime)));
          tempFreeRooms.push(new FreeRoom(data.room, startOfDay(addDays(this.startTime, 1)), endOfDay(addDays(this.startTime, 1))));
          tempFreeRooms.push(new FreeRoom(data.room, startOfDay(addDays(this.startTime, 2)), endOfDay(addDays(this.startTime, 2))));
        } else {
          let last = new Date(this.startTime.getTime());
          do {
            const temp = data.occupations.shift();
            if (temp.from < last) {
              last = new Date(temp.to);
            } else if (isSameDay(last, temp.from)) {
              tempFreeRooms.push(new FreeRoom(data.room, last, temp.from));
            } else {
              tempFreeRooms.push(new FreeRoom(data.room, last, endOfDay(last)));
              tempFreeRooms.push(new FreeRoom(data.room, startOfDay(addDays(last, 1)), temp.from));
            }
            last = new Date(temp.to);
          } while (data.occupations.length > 0);
          tempFreeRooms.push(new FreeRoom(data.room, last, endOfDay(last)));
        }
        return tempFreeRooms;
      }),
      toArray()
    ).subscribe((freeRooms: FreeRoom[]) => {
      this.freeRoomsArray = _collection.sortBy(this.freeRoomsArray.concat(freeRooms), (freeRoom: FreeRoom) => freeRoom.from);
    });
  }

  book(freeRoom: FreeRoom) {
    const dialogRef = this.dialog.open(BookingDialogComponent, {
      width: '300px',
      data: freeRoom
    });
    dialogRef.afterClosed().subscribe((result: Booking) => {
      if (result) {
        this.bookingService.makeBooking(result);
        this.router.navigate(['/my-area']);
      }
    });
  }

}

class OccupiedTimes {
  constructor(
    public roomId: string,
    public from: Date,
    public to: Date
  ) { }
}

class FreeRoom {
  constructor(
    public name: string,
    public from: Date,
    public to: Date
  ) { }
}
