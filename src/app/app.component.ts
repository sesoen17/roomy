import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AuthService, User } from './services/auth/auth.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BookingService, Booking } from './services/booking/booking.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  user$: BehaviorSubject<User>;

  constructor(
    private router: Router,
    private authServices: AuthService,
    private bookingService: BookingService
  ) {
    this.user$ = this.authServices.user$;
  }
}
