import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService, User } from '../services/auth/auth.service';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfileComponent implements OnInit {
  user$: BehaviorSubject<User>;
  userFormGroup: FormGroup;
  userFormMatcher: UserFormErrorStateMatcher;
  showPassword: boolean;
  register: boolean;

  constructor(
    private router: Router, private formBuilder: FormBuilder,
    private authService: AuthService
  ) {
    this.user$ = this.authService.user$;
  }

  ngOnInit() {
    this.userFormGroup = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.email,
        Validators.pattern('.*unibz\.it')
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(20)
      ])],
      first: [''],
      last: [''],
      phone: [''],
      faculty: ['']
    });

    this.userFormMatcher = new UserFormErrorStateMatcher();
    this.showPassword = false;
  }

  submit(tempUser: User) {
    if (this.register) {
      tempUser.picture = 'http://dev.testwpinstall.com.php56-12.phx1-2.websitetestlink.com/img/user.jpg';
      this.authService.register(tempUser);
    } else {
      this.authService.login(tempUser.email, tempUser.password);
    }
  }

  logout() {
    this.authService.logout(this.user$.value.userId);
    this.router.navigate(['/free']);
  }

}

/** Error when invalid control is dirty, touched, or submitted. */
export class UserFormErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
