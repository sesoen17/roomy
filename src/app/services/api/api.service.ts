import { ApiRoomEvent } from './api.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { format, addDays } from 'date-fns/esm';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import _collection from 'lodash-es/collection';
import _array from 'lodash-es/array';

@Injectable()
export class ApiService {
  // API documentation: https://webservices.scientificnet.org/rest/uisdata/#timetable
  private readonly API_URL = 'https://cors-anywhere.herokuapp.com/https://webservices.scientificnet.org/rest/uisdata/api/v1/timetable';

  apiRoomEvents$: BehaviorSubject<ApiRoomEvent[]>;
  universityRooms$: BehaviorSubject<string[]>;

  constructor(private httpClient: HttpClient) {
    this.apiRoomEvents$ = new BehaviorSubject<ApiRoomEvent[]>([]);
    this.universityRooms$ = new BehaviorSubject<string[]>([]);

    this.get(new Date(), addDays(new Date(), 14));
  }

  private get(fromDate?: Date, toDate?: Date) {
    let params = fromDate ? new HttpParams().set('fromDate', format(fromDate, 'YYYY-MM-DD')) : new HttpParams();
    params = toDate ? params.set('toDate', format(toDate, 'YYYY-MM-DD')) : params;

    this.httpClient.get<ApiResponse>(this.API_URL, {
      params: params
    })
      .subscribe(
      (data: ApiResponse) => {
        let tempArray: ApiRoomEvent[] = [];
        for (const entryList of data.EntriesList) {
          tempArray = tempArray.concat(entryList.Events);
        }

        this.parseDates(tempArray);
        tempArray = this.filterEvents(tempArray);
        this.apiRoomEvents$.next(tempArray);
        // console.log('UniBz events: ', tempArray);

        this.extractRooms(tempArray);
      },
      err => console.error(err));
  }

  private parseDates(eventList: ApiRoomEvent[]) {
    for (const entry of eventList) {
      entry.FromDate = new Date(entry.FromDate);
      entry.ToDate = new Date(entry.ToDate);
    }
  }

  private filterEvents(eventList: ApiRoomEvent[]): ApiRoomEvent[] {
    return _collection.filter(eventList, (apiRoomEvent: ApiRoomEvent) => {
      return this.hasRoom(apiRoomEvent) && this.isInBz(apiRoomEvent);
    });
  }

  // Filter out events without a room (Internships)
  private hasRoom(event: ApiRoomEvent): boolean {
    return event.Room ? true : false;
  }

  // FIlter out events from Brixen and Bruneck
  private isInBz(event: ApiRoomEvent): boolean {
    return event.Room.indexOf('BZ ') > -1 ? true : false;
  }

  private extractRooms(eventList: ApiRoomEvent[]) {
    let tempRooms: string[] = [];
    for (const event of eventList) {
      tempRooms.push(event.Room);
    }
    tempRooms = _collection.sortBy(tempRooms);
    tempRooms = _array.sortedUniq(tempRooms);

    this.universityRooms$.next(tempRooms);
    // console.log('UniBz rooms: ', tempRooms);
  }
}

interface ApiResponse {
  EntriesCount: any;
  EntriesList: EntryList[];
}

interface EntryList {
  Date: Date;
  Events: ApiRoomEvent[];
}

export interface ApiRoomEvent {
  Id: number;
  Room: string;
  FromDate: Date;
  ToDate: Date;
  Description: string;
  Type: string;
}
