import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { map } from 'rxjs/operators/map';
import { concatMap } from 'rxjs/operators/concatMap';
import { toArray } from 'rxjs/operators/toArray';
import { from } from 'rxjs/observable/from';
import { range } from 'rxjs/observable/range';
import { format, addDays } from 'date-fns/esm';
import { User, AuthService } from '../auth/auth.service';
import _collection from 'lodash-es/collection';
import _array from 'lodash-es/array';

@Injectable()
export class BookingService {
  private readonly BACKEND_URL = 'https://cors-anywhere.herokuapp.com/https://roomie-server.herokuapp.com/api/room';
  private readonly PARTICIPATION_URL_PREFIX = 'https://cors-anywhere.herokuapp.com/https://roomie-server.herokuapp.com/api/booking/';
  private readonly PARTICIPATION_URL_SUFIX = '/participants';

  bookings$: BehaviorSubject<Booking[]>;
  bookingRooms$: BehaviorSubject<string[]>;

  private bookings: Booking[];
  private headers: HttpHeaders;

  constructor(private httpClient: HttpClient, private authService: AuthService) {
    this.bookings = [];
    this.headers = (new HttpHeaders()).append('Content-Type', 'application/json');

    this.bookings$ = new BehaviorSubject<Booking[]>(this.bookings);
    this.bookingRooms$ = new BehaviorSubject<string[]>([]);

    this.getBookings(new Date());
  }

  private getBookings(date: Date) {
    range(0, 3).pipe(
      concatMap((offset: number) => this.httpClient.get<Booking[]>(this.BACKEND_URL + '/booked', {
        params: new HttpParams().set('date', format(addDays(date, offset), 'YYYY-MM-DD')),
        headers: this.headers
      })
        .pipe(
        concatMap(data => from(data)),
        map(this.addDates)
        )),
      concatMap((booking: Booking) => {
        return this.httpClient.get<User[]>(this.PARTICIPATION_URL_PREFIX + booking.bookingId + this.PARTICIPATION_URL_SUFIX)
          .pipe(
          map((participants: User[]) => {
            booking.participants = participants;
            return booking;
          })
          );
      }),
      toArray()
    ).subscribe((bookings: Booking[]) => {
      // console.log('Bookings: ', bookings);
      const now = new Date();
      this.bookings = bookings.filter((booking: Booking) => now < booking.toDate);
      this.bookings$.next(bookings);
      this.extractRooms(bookings);
    }, err => console.error(err));
  }

  getMyBookings(): Observable<Booking[]> {
    return this.httpClient.get<Booking[]>(this.BACKEND_URL + '/booked', {
      params: new HttpParams()
        .set('userId', this.authService.user$.value.userId.toString()),
      headers: this.headers
    })
      .pipe(
      concatMap(data => from(data)),
      map(this.addDates),
      concatMap((booking: Booking) => {
        return this.httpClient.get<User[]>(this.PARTICIPATION_URL_PREFIX + booking.bookingId + this.PARTICIPATION_URL_SUFIX)
          .pipe(
          map((participants: User[]) => {
            booking.participants = participants;
            return booking;
          })
          );
      }),
      toArray()
      );
  }

  private addDates(booking: Booking) {
    booking.fromDate = new Date(
      booking.year,
      booking.month - 1,
      booking.day,
      booking.fromTime / 100,
      booking.fromTime % 100
    );
    booking.toDate = new Date(
      booking.year,
      booking.month - 1,
      booking.day,
      booking.toTime / 100,
      booking.toTime % 100
    );
    return booking;
  }

  private extractRooms(bookings: Booking[]) {
    let tempRooms: string[] = [];
    for (const booking of bookings) {
      tempRooms.push(booking.roomId);
    }
    tempRooms = _collection.sortBy(tempRooms);
    tempRooms = _array.sortedUniq(tempRooms);

    this.bookingRooms$.next(tempRooms);
    // console.log('Booking rooms: ', tempRooms);
  }

  makeBooking(booking: Booking) {
    this.httpClient.post<Booking>(this.BACKEND_URL + '/book', JSON.stringify(booking), { headers: this.headers }).pipe(
      map(this.addDates),
      concatMap((tempBooking: Booking) => {
        return this.httpClient.get<User[]>(this.PARTICIPATION_URL_PREFIX + tempBooking.bookingId + this.PARTICIPATION_URL_SUFIX)
          .pipe(
          map((participants: User[]) => {
            tempBooking.participants = participants;
            return tempBooking;
          })
          );
      })
    ).subscribe((booked: Booking) => {
      this.bookings.push(booked);
      this.bookings$.next(this.bookings);
    }, err => console.error(err));
  }

  deleteBooking(bookingId: number): Promise<void> {
    return new Promise((resolve, reject) => {
      this.httpClient.delete(this.BACKEND_URL + '/cancel', {
        params: new HttpParams()
          .set('bookingId', bookingId.toString())
          .set('userId', this.authService.user$.value.userId.toString()),
        headers: this.headers
      })
        .subscribe(() => {
          for (const i in this.bookings) {
            if (this.bookings[i].bookingId === bookingId) {
              if (this.bookings[i].userId === this.authService.user$.value.userId) {
                this.bookings.splice(+i, 1);
                this.bookings$.next(this.bookings);
                resolve();
                return;
              } else {
                for (const ii in this.bookings[i].participants) {
                  if (this.bookings[i].participants[ii].userId === this.authService.user$.value.userId) {
                    this.bookings[i].participants.splice(+ii, 1);
                    this.bookings$.next(this.bookings);
                    resolve();
                    return;
                  }
                }
              }
            }
          }
        }, err => {
          console.error(err);
          reject();
        });
    });
  }

  join(bookingId: number, user: User) {
    this.httpClient.post(this.BACKEND_URL + '/join', {
      bookingId: bookingId,
      userId: user.userId
    })
      .subscribe(() => {
        for (const booking of this.bookings) {
          if (booking.bookingId === bookingId) {
            booking.participants.push(user);
            this.bookings$.next(this.bookings);
            break;
          }
        }
      }, err => console.error(err));
  }

}

export class Booking {
  bookingId: number;
  day: number;
  fromTime: number;
  month: number;
  roomId: string;
  title: string;
  toTime: number;
  userId: number;
  year: number;
  fromDate: Date;
  toDate: Date;
  participants: User[];
}
