import { Component, OnInit } from '@angular/core';
import { AuthService, User } from '../services/auth/auth.service';
import { BookingService, Booking } from '../services/booking/booking.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-booked',
  templateUrl: './booked.component.html',
  styleUrls: ['./booked.component.scss']
})
export class BookedComponent implements OnInit {
  bookings$: BehaviorSubject<Booking[]>;
  user$: BehaviorSubject<User>;

  constructor(private authService: AuthService, private bookingService: BookingService) { }

  ngOnInit() {
    this.bookings$ = this.bookingService.bookings$;
    this.user$ = this.authService.user$;
  }

  join(bookingId: number) {
    this.bookingService.join(bookingId, this.user$.value);
  }

  participating(booking: Booking): boolean {
    return booking.participants.some((user: User) => user.userId === this.user$.value.userId);
  }

  plusMoreTooltip(booking: Booking): string {
    let temp = '';
    for (let i = 2; i < booking.participants.length; i++) {
      temp += booking.participants[i].first + ' ' + booking.participants[i].last + ', ';
    }
    temp = temp.slice(0, temp.length - 2);
    return temp;
  }

}
