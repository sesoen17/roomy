import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './services/api/api.service';
import { BrowserModule, } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, ErrorHandler } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatToolbarModule, MatGridListModule, MatCardModule, MatListModule, MatInputModule, MatButtonModule, MatIconModule,
  MatTooltipModule, MatDialogModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { FreeRoomsComponent } from './free/free.component';
import { registerLocaleData } from '@angular/common';
import localeDE from '@angular/common/locales/de';
import { AuthService } from './services/auth/auth.service';
import { BookingService } from './services/booking/booking.service';
import { ProfileComponent } from './profile/profile.component';
import { BookedComponent } from './booked/booked.component';
import { BookingDialogComponent } from './booking-dialog/booking-dialog.component';
import { MyAreaComponent } from './my-area/my-area.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import * as Raven from 'raven-js';

Raven
  .config('https://4440141124924c6896b20b26c3197297@sentry.fallenriders.eu/4')
  .install();

export class RavenErrorHandler implements ErrorHandler {
  handleError(error: any): void {
    Raven.captureException(error);
  }
}

@NgModule({
  declarations: [
    AppComponent,
    FreeRoomsComponent,
    ProfileComponent,
    BookedComponent,
    BookingDialogComponent,
    MyAreaComponent
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatGridListModule,
    MatCardModule,
    MatListModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule
  ],
  providers: [
    ApiService,
    AuthService,
    BookingService,
    { provide: ErrorHandler, useClass: RavenErrorHandler }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    BookingDialogComponent
  ]
})
export class AppModule { }

registerLocaleData(localeDE);
